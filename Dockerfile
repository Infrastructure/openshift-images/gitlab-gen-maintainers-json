FROM registry.access.redhat.com/ubi9/python-311:1

ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD gen-maintainers-json.py /usr/local/bin/gen-maintainers-json.py
CMD ["python", "/usr/local/bin/gen-maintainers-json.py"]
