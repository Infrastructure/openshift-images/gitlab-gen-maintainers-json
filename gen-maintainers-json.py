#!/usr/bin/python3

import json
import os
import sys
from optparse import OptionParser

import gitlab
from kubernetes import client, config

gl = gitlab.Gitlab(
    "https://gitlab.gnome.org", os.getenv("GITLAB_PRIVATE_RW_TOKEN"), api_version=4
)

usage = "usage: %prog [options]"
parser = OptionParser(usage)
parser.add_option(
    "--project_id",
    "-p",
    action="store",
    type=int,
    dest="project_id",
    help="GitLab Project ID",
)
parser.add_option(
    "--configmap-name",
    "-c",
    action="store",
    type=str,
    dest="configmap_name",
    help="Name of the ConfigMap to update",
)
parser.add_option(
    "--namespace",
    "-n",
    action="store",
    type=str,
    dest="namespace",
    default="default",
    help="Kubernetes namespace for the ConfigMap",
)

(options, args) = parser.parse_args()


def find_ldap_uid(gl_user_object):
    user = gl.users.get(gl_user_object.attributes["id"])
    if len(user.attributes["identities"]) > 0:
        for index, _ in enumerate(user.attributes["identities"]):
            provider = user.attributes["identities"][index]["provider"]
            if provider == "ldapmain":
                return (
                    user.attributes["identities"][index]["extern_uid"]
                    .split(",")[0]
                    .replace("uid=", "")
                )


def gen_maints_list(gl_project_id):
    result = {}
    group = gl.groups.get(gl_project_id, with_projects=False)
    projects = group.projects.list(all=True, with_shared=False)
    subgroups = group.subgroups.list()
    for subgroup in subgroups:
        s = gl.groups.get(subgroup.id)
        subprojects = s.projects.list()
        projects.extend(subprojects)

    for p in projects:
        _p = gl.projects.get(p.attributes["id"])
        members = _p.members.list(get_all=True)
        result[_p.attributes["path_with_namespace"]] = {"maintainers": []}
        for maintainer in members:
            if maintainer.attributes["access_level"] == 40:
                result[_p.attributes["path_with_namespace"]]["maintainers"].append(
                    {
                        "gitlab_username": maintainer.attributes["username"],
                        "ldap_username": find_ldap_uid(maintainer),
                    }
                )
    return result


def update_configmap(data, configmap_name, namespace):
    config.load_incluster_config()
    api = client.CoreV1Api()
    try:
        existing_configmap = api.read_namespaced_config_map(configmap_name, namespace)
        existing_configmap.data["maintainers.json"] = json.dumps(data)
        api.patch_namespaced_config_map(configmap_name, namespace, existing_configmap)
        print(f"ConfigMap '{configmap_name}' updated successfully.")
    except client.exceptions.ApiException as e:
        if e.status == 404:
            configmap = client.V1ConfigMap(
                metadata=client.V1ObjectMeta(name=configmap_name),
                data={"maintainers.json": json.dumps(data)},
            )
            api.create_namespaced_config_map(namespace, configmap)
            print(f"ConfigMap '{configmap_name}' created successfully.")
        else:
            print(f"Error updating ConfigMap: {e}")


def main():
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    maintainers_data = gen_maints_list(options.project_id)
    update_configmap(maintainers_data, options.configmap_name, options.namespace)


if __name__ == "__main__":
    main()
